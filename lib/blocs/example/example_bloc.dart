import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:bloc_example/models/example/example_parse.dart';
import 'package:bloc_example/repository/example/example.dart';
import 'package:equatable/equatable.dart';

part 'example_event.dart';
part 'example_state.dart';

class ExampleBloc extends Bloc<ExampleEvent, ExampleState> {

  @override
  Stream<ExampleState> mapEventToState(
    ExampleEvent event,
  ) async* {
    if(event is OnFetchExampleEvent){
      yield* _mapFetchToState(event);
    }
  }


  Stream<ExampleState> _mapFetchToState(OnFetchExampleEvent event) async* {
    yield LoadingExampleState();
    var data = await getDataExample();
    if(data != null){
      yield SuccessExampleState(
          status: ExampleParse.fromJson(data),
      );
    }else{
      yield ErrorExampleState(message: 'error', code: 10);
    }
  }

  @override
  // TODO: implement initialState
  ExampleState get initialState => ExampleInitial();
}

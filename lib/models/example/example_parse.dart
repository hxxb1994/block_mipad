// To parse this JSON data, do
//
//     final exampleParse = exampleParseFromJson(jsonString);

import 'dart:convert';

ExampleParse exampleParseFromJson(String str) => ExampleParse.fromJson(json.decode(str));

String exampleParseToJson(ExampleParse data) => json.encode(data.toJson());

class ExampleParse {
  ExampleParse({
    this.test1,
    this.test2,
  });

  final String test1;
  final String test2;

  factory ExampleParse.fromJson(Map<String, dynamic> json) => ExampleParse(
    test1: json["test1"] == null ? null : json["test1"],
    test2: json["test2"] == null ? null : json["test2"],
  );

  Map<String, dynamic> toJson() => {
    "test1": test1 == null ? null : test1,
    "test2": test2 == null ? null : test2,
  };
}
import 'package:bloc_example/blocs/example/example_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  ExampleBloc _exampleBloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _exampleBloc = ExampleBloc();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _exampleBloc.close();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(

        title: Text(widget.title),
      ),
      body: _buildBodyWidget(context),
    );
  }

  Widget _buildBodyWidget(context){
    return BlocProvider(
      create: (context)=>_exampleBloc..add(OnFetchExampleEvent()),
      child: BlocListener<ExampleBloc, ExampleState>(
        listener: (context, state){

        },
        child: BlocBuilder<ExampleBloc, ExampleState>(
          builder: (context, state){
            if(state is LoadingExampleState){
              return Center(
                child: Text("loading...."),
              );
            }
            else if(state is ErrorExampleState){
              return Center(
                child: Text("${state.message}"),
              );
            }else if(state is SuccessExampleState){
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'success: ',
                    ),
                    Text(
                      '${state.status.test1}',
                    ),
                    Text(
                      '${state.status.test2}',
                    ),
                  ],
                ),
              );
            }
            return Container();
          },
        ),
      ),
    );
  }
}